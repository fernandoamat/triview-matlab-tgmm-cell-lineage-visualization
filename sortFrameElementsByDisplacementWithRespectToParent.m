%sorts elements of a frame by size

function [sortIdxList, aux ]=sortFrameElementsByDisplacementWithRespectToParent(blobStructFrame,blobStructFrameChildren,anisotropy)


vol=zeros(length(blobStructFrame),1);
for ii=1:length(blobStructFrame)
    
    if(isempty(blobStructFrame(ii).frame))
        vol(ii)=0;
    else
        if(blobStructFrame(ii).center(1)<-1e31)%dead cell
            vol(ii)=0;
        else
            ch=blobStructFrame(ii).solutions(1).childrenIdx;
            
            if(length(ch) == 2 )
                if(blobStructFrameChildren(ch(2)+1).center(1)>-1)%parent not dead
                    vol(ii)=norm((blobStructFrameChildren(ch(2)+1).center-blobStructFrame(ii).center).*[1 1 anisotropy]);
                end
            end
        end
    end
end


[aux, sortIdxList]=sort(vol,'descend');%returns 